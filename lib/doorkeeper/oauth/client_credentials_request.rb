# frozen_string_literal: true

module Doorkeeper
  module OAuth
    class ClientCredentialsRequest < BaseRequest
      attr_accessor :server, :client, :original_scopes
      attr_reader :response, :payload
      attr_writer :issuer

      alias error_response response

      delegate :error, to: :issuer

      def issuer
        @issuer ||= Issuer.new(server, Validation.new(server, self))
      end

      def initialize(server, client, parameters = {})
        @client = client
        @server = server
        @response = nil
        @original_scopes = parameters[:scope].presence || client&.scopes&.to_s
        @payload = parameters.fetch(:payload, {})
      end

      def access_token
        issuer.token
      end

      private

      def valid?
        issuer.create(client, scopes, payload: payload)
      end
    end
  end
end
